
def generateMap(details):
  class Struct:
    def __init__(self, **entries):
      self.__dict__.update(entries)
  return Struct(**{
    "width": 1200,
    "height": 600,
    "obstacles": (
      ((0, 0), (1200, 0), (1200, 600), (0, 600)),
      ((200, 200), (300, 200), (300, 300)),
    )
  })