#!/usr/bin/python

import math

def eucDistanceSquare(p1, p2):
  return (p1[0] - p2[0])**2 + (p1[1] - p2[1])**2

def circleLineCollision(center, radius, p1, p2):
  r2 = radius * radius
  if r2 >= eucDistanceSquare(center, p1):
    return True
  if r2 >= eucDistanceSquare(center, p2):
    return True
  lineLengthSqr = eucDistanceSquare(p1, p2)
  t = ((center[0] - p1[0]) * (p2[0] - p1[0]) +
    (center[1] - p1[1]) * (p2[1] - p1[1])) / lineLengthSqr
  if 0 < t < 1:
    closestPoint = (p1[0] + t * (p2[0] - p1[0]), p1[1] + t * (p2[1] - p1[1]))
    if r2 >= eucDistanceSquare(center, closestPoint):
      return True
  return False

def lineCircleIntersection(center, radius, p1, p2):
  """finds the intersection with the circle that's closest to p1"""
  """
LAB = sqrt( (Bx-Ax)²+(By-Ay)² )
// compute the direction vector D from A to B
Dx = (Bx-Ax)/LAB
Dy = (By-Ay)/LAB
// compute the distance between the points A and E, where
// E is the point of AB closest the circle center (Cx, Cy)
t = Dx*(Cx-Ax) + Dy*(Cy-Ay)    

// compute the coordinates of the point E
Ex = t*Dx+Ax
Ey = t*Dy+Ay

// compute the euclidean distance between E and C
LEC = sqrt((Ex-Cx)²+(Ey-Cy)²)

// test if the line intersects the circle
if( LEC < R )
{
    // compute distance from t to circle intersection point
    dt = sqrt( R² - LEC²)

    // compute first intersection point
    Fx = (t-dt)*Dx + Ax
    Fy = (t-dt)*Dy + Ay

    // compute second intersection point
    Gx = (t+dt)*Dx + Ax
    Gy = (t+dt)*Dy + Ay
}

// else test if the line is tangent to circle
else if( LEC == R )
    // tangent point to circle is E

else
    // line doesn't touch circle
  """
  r2 = radius * radius
  lineLengthSqr = eucDistanceSquare(p1, p2)
  t = ((center[0] - p1[0]) * (p2[0] - p1[0]) +
    (center[1] - p1[1]) * (p2[1] - p1[1])) / lineLengthSqr
  if 0 <= t:
    closestPoint = (p1[0] + t * (p2[0] - p1[0]), p1[1] + t * (p2[1] - p1[1]))
    closestDistance = eucDistanceSquare(center, closestPoint)
    if r2 > closestDistance:
      dt = math.sqrt(r2 - closestDistance) / math.sqrt(lineLengthSqr)
      return (p1[0] + (t - dt) * (p2[0] - p1[0]), p1[1] + (t - dt) * (p2[1] - p1[1]))
    if r2 == closestDistance:
      return closestPoint
  return None

def lineLineIntersection(p1, p2, q1, q2):
  s1 = (p2[0] - p1[0], p2[1] - p1[1])
  s2 = (q2[0] - q1[0], q2[1] - q1[1])
# s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
# t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

  denom = (-s2[0] * s1[1] + s1[0] * s2[1])
  if denom == 0:
    return None

  s = (-s1[1] * (p1[0] - q1[0]) + s1[0] * (p1[1] - q1[1])) / denom
  t = (s2[0] * (p1[1] - q1[1]) - s2[1] * (p1[0] - q1[0])) / denom
  
  if 0 <= s <= 1 and 0 <= t <= 1:
    return (p1[0] + t * s1[0], p1[1] + t * s1[1])
  return None
