#!/usr/bin/python

class Struct:
  def __init__(self, **entries):
    self.__dict__.update(entries)

class AutomaticDriver:
  def __init__(self):
    pass

  @staticmethod
  def needsMeasurements():
    return True
  
  def drive(self, measurements, navigation):
    return Struct(**{
      "acceleration": .1,
      "turn": 1
    })

class DummyDriver:
  def __init__(self):
    pass

  @staticmethod
  def needsMeasurements():
    return False
  
  def drive(self, measurements, navigation):
    return Struct(**{
      "acceleration": 0,
      "turn": 0
    })

class ManualDriver:
  def __init__(self, tk):
    tk.bind('<KeyPress-Up>', lambda event: self.setAccelerate(1))
    tk.bind('<KeyPress-Down>', lambda event: self.setAccelerate(-1))
    tk.bind('<KeyRelease-Up>', lambda event: self.setAccelerate(0))
    tk.bind('<KeyRelease-Down>', lambda event: self.setAccelerate(0))
    tk.bind('<KeyPress-Left>', lambda event: self.setTurn(1))
    tk.bind('<KeyPress-Right>', lambda event: self.setTurn(-1))
    tk.bind('<KeyRelease-Left>', lambda event: self.setTurn(0))
    tk.bind('<KeyRelease-Right>', lambda event: self.setTurn(0))
    self.accelerate = 0
    self.turn = 0

  @staticmethod
  def needsMeasurements():
    return False
  
  def setAccelerate(self, val):
    self.accelerate = val
  
  def setTurn(self, val):
    self.turn = val
  
  def drive(self, measurements, navigation):
    return Struct(**{
      "acceleration": self.accelerate,
      "turn": self.turn
    })


