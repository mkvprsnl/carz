#!/usr/bin/python

from tkinter import *
import math
import itertools
import mapGen
from driver import AutomaticDriver, DummyDriver, ManualDriver
from physics import *

TIME_INTERVAL = 40
CAR_SIZE = 10
CAR_ACCELERATION = 0.1
DRAG = 0.99
MAX_TURN_RATE = 0.3
SELECTION_TAG = "selection"
SELECTION_TEXT = "selection-text"
SELECTION_RAYS = "selection-rays"
PROBE_RAYS = [0.0, 0.1, -0.1, 0.3, -0.3, math.pi / 2, -math.pi / 2, math.pi]
PROBE_RAY_LENGTH = 255

def createCanvas(tk, width, height):
  canvas = Canvas(tk)
  canvas.xview_moveto(0)
  canvas.yview_moveto(0)
  canvas["bg"] = "white"
  canvas["width"] = width
  canvas["height"] = height
  
  return canvas

def createCar(position, heading):
  class Struct:
    def __init__(self, **entries):
      self.__dict__.update(entries)
  carData = {
    "posX": position[0],
    "posY": position[1],
    "heading": heading,
    "speed": 0.0,
    "acceleration": 0,
    "turn": 0,
  }
  return Struct(**carData)

def carBox(carPos):
  FROM_CENTER = CAR_SIZE / 2
  return (carPos[0] - FROM_CENTER, carPos[1] - FROM_CENTER,
    carPos[0] + FROM_CENTER, carPos[1] + FROM_CENTER)

def carHeadingIndicator(car):
  FROM_CENTER = CAR_SIZE# / 2
  direction = headingToUnitVector(car.heading)
  return (car.posX, car.posY, car.posX + direction[0] * FROM_CENTER, car.posY + direction[1] * FROM_CENTER)

def headingToUnitVector(heading):
  """
    0 is vertical down, CCW from there
  """
  return (math.sin(heading), math.cos(heading))

def applyTurningCurve(normalizedSpeed):
  """
    left parabola: -100x^2/9+20x/3
    right parabola: −90x^2/49+54x/49+409/490
    mid point: 0.3
  """
  def leftParabola(x):
    return -100*x*x/9+20*x/3
  def rightParabola(x):
    return -90*x*x/49+54*x/49+409/490
  if normalizedSpeed < 0.3:
    return leftParabola(normalizedSpeed)
  else:
    return rightParabola(normalizedSpeed)

def turnCar(car):
  """
    (MAX + ACCEL) * DRAG = MAX
    MAX = -A * D / (D - 1)
  """
  MAX_SPEED = -CAR_ACCELERATION * DRAG / (DRAG - 1)
  turnCoefficient = applyTurningCurve(abs(car.speed) / MAX_SPEED)
  car.heading += turnCoefficient * MAX_TURN_RATE * car.turn * math.copysign(1, car.speed)

def carVelocity(car):
  direction = headingToUnitVector(car.heading)
  return (direction[0] * car.speed, direction[1] * car.speed)

def calculateCarCollisions(collisionData):
  collided = set()
  indexes = list(collisionData.keys())
  MIN_DISTANCE = CAR_SIZE * CAR_SIZE
  for i in range(len(indexes)):
    indexI = indexes[i]
    for j in range(i + 1, len(indexes)):
      indexJ = indexes[j]
      for ci in collisionData[indexI]:
        for cj in collisionData[indexJ]:
          if MIN_DISTANCE > eucDistanceSquare(ci, cj):
            collided.add(indexI)
            collided.add(indexJ)
            break
        else: continue
        break
  return collided

def generateWalls(obstacle):
  for p1, p2 in zip(obstacle, obstacle[1:] + (obstacle[0],)):
    yield(p1, p2)

def calculateObstacleCollisions(carData, obstacles):
  collided = set()
  for index in carData:
    for circle in carData[index]:
      for obstacle in obstacles:
        for p1, p2 in generateWalls(obstacle):
          if circleLineCollision(circle, CAR_SIZE / 2, p1, p2):
            collided.add(index)
            break
        else: continue
        break
      else: continue
      break
  return collided

def generateRays(carPos, carHeading):
  p1 = carPos
  for rayHeading in PROBE_RAYS:
    p2 = headingToUnitVector(carHeading + rayHeading)
    yield (p1, (p1[0] + p2[0] * PROBE_RAY_LENGTH, p1[1] + p2[1] * PROBE_RAY_LENGTH))

class Carz():
  def __init__(self, tk):
    self.tk = tk
    tk.title('Carz Simulation')
    
    self.map = mapGen.generateMap(None)
    
    self.canvas = createCanvas(tk, self.map.width, self.map.height)
    self.canvas.grid()
    
    [self.canvas.create_polygon(
      *(coord for pair in obstacle for coord in pair), fill="", outline="black") for obstacle in
      self.map.obstacles]
    
    self.carData = [createCar((100, 100), 0.0), createCar((150, 150), 3.0)]
    
    self.carDots = [(self.canvas.create_oval(*carBox((car.posX, car.posY)),
                            fill = "red", outline = "black"),
                    self.canvas.create_line(*carHeadingIndicator(car), arrow=LAST))
                      for car in self.carData]
    self.drivers = [DummyDriver(), AutomaticDriver()]
    
    tk.bind('<p>', lambda event: self.pausePlay())
    tk.bind('<s>', lambda event: self.step())
    
    self.selected = None
    self.canvas.bind('<Button-1>', lambda event: self.selectCar((event.x, event.y)))
    self.canvas.create_oval(0, 0, 10, 10, fill = "", outline = "red", state=HIDDEN, tag=SELECTION_TAG)
    self.canvas.create_text(10, 10, fill = "black", anchor=NW, state=HIDDEN, tags=(SELECTION_TAG, SELECTION_TEXT))

    self.tick = 0
    self.livePlay = False
    if self.livePlay:
      self.moveCarz()
    
    #self.canvas.bind('<Motion>', lambda event: self.intersectTest((event.x, event.y)))

  def intersectTest(self, coords):
    self.canvas.delete("kaka")
    
    p1 = self.map.width / 2, self.map.height / 2
    p2 = coords

    self.canvas.create_line(*p1, *p2, tag = "kaka")
    print("Intersections from (%d, %d) to (%d, %d):" % (*p1, *p2))
    for obstacle in self.map.obstacles:
      for q1, q2 in generateWalls(obstacle):
        cross = lineLineIntersection(p1, p2, q1, q2)
        if cross != None:
          print("  line: (%f, %f)" % cross)
          self.canvas.create_oval(cross[0] - 2, cross[1] - 2, cross[0] + 2, cross[1] + 2, tag="kaka", outline="red")
    for car in self.carData:
      cross = lineCircleIntersection((car.posX, car.posY), CAR_SIZE / 2, p1, p2)
      if cross != None:
        print("  car : (%f, %f)" % cross)
        self.canvas.create_oval(cross[0] - 2, cross[1] - 2, cross[0] + 2, cross[1] + 2, tag="kaka", outline="green")

  def collectMeasurements(self, carIndex):
    car = self.carData[carIndex]
    p1 = (car.posX, car.posY)
    result = []
    for ray in generateRays(p1, car.heading):
      carCollisions = (lineCircleIntersection((oCar.posX, oCar.posY), CAR_SIZE / 2, *ray) for oCar in self.carData if car is not oCar)
      wallCollisions = (lineLineIntersection(*ray, *wall) for obstacle in self.map.obstacles for wall in generateWalls(obstacle))
      sortedCollisions = sorted(((point, eucDistanceSquare(p1, point)) for point in itertools.chain(carCollisions, wallCollisions) if point is not None),
        key = lambda x: x[1])
      if len(sortedCollisions) != 0:
        result.append(sortedCollisions[0][0])
      else:
        result.append(ray[1])
    return result
  
  def pausePlay(self):
    self.livePlay = not self.livePlay
    if self.livePlay:
      self.moveCarz()
  
  def step(self):
    if not self.livePlay:
      self.moveCarz()

  def selectCar(self, coords):
    if self.selected != None:
      self.drivers[self.selected] = AutomaticDriver()
    for i in range(len(self.carData)):
      car = self.carData[i]
      if eucDistanceSquare(coords, (car.posX, car.posY)) < CAR_SIZE * CAR_SIZE:
        self.selected = i
        self.canvas.coords(SELECTION_TAG, *carBox((car.posX, car.posY)))
        self.canvas.itemconfigure(SELECTION_TAG, state=NORMAL)
        self.drivers[i] = ManualDriver(self.tk)
        break
      else:
        self.selected = None
        self.canvas.itemconfigure(SELECTION_TAG, state=HIDDEN)
          
  
  def driveCarz(self):
    for i in range(len(self.drivers)):
      driver = self.drivers[i]
      measurements = self.collectMeasurements(i) if driver.needsMeasurements() else None
      commands = driver.drive(measurements, None)
      assert -1 <= commands.acceleration <= 1
      assert -1 <= commands.turn <= 1
      self.carData[i].acceleration = commands.acceleration * CAR_ACCELERATION
      self.carData[i].turn = commands.turn * MAX_TURN_RATE
  
  def moveCarz(self):
    self.tick += 1
    
    self.driveCarz()

    collision = {}
    for i in range(len(self.carData)):
      car = self.carData[i]
      turnCar(car)

      car.speed += car.acceleration
      car.speed *= DRAG
      velocity = carVelocity(car)
      collision[i] = [(car.posX, car.posY)]
      car.posX += velocity[0]
      car.posY += velocity[1]
      collision[i].append((car.posX, car.posY))

      self.canvas.coords(self.carDots[i][0], *carBox((car.posX, car.posY)))
      self.canvas.coords(self.carDots[i][1], *carHeadingIndicator(car))
      if self.selected == i:
        self.canvas.coords(SELECTION_TAG, *carBox((car.posX, car.posY)))
        self.canvas.itemconfigure(SELECTION_TEXT, text="speed: %f, heading: %f" % (car.speed, car.heading))
        probeLines = self.collectMeasurements(i)
        self.canvas.delete(SELECTION_RAYS)
        for line in probeLines:
          self.canvas.create_line(car.posX, car.posY, *line, tags=(SELECTION_TAG, SELECTION_RAYS))

    toRemove = sorted(list(set.union(calculateCarCollisions(collision), calculateObstacleCollisions(collision, self.map.obstacles))))
    if 0 != len(toRemove):
      for index in reversed(toRemove):
        del self.carData[index]
        del self.drivers[index]
        self.canvas.delete(self.carDots[index][0])
        self.canvas.delete(self.carDots[index][1])
        del self.carDots[index]
        if index == self.selected:
          self.canvas.itemconfigure(SELECTION_TAG, state=HIDDEN)
          self.selected = None

    if self.livePlay:
      self.tk.after(TIME_INTERVAL, self.moveCarz)
    

if __name__ == '__main__':
  tk = Tk()
  carz = Carz(tk)
  tk.mainloop()

  
